sap.ui.define([
	"parasus/products/test/unit/controller/ProductList.controller",
     "parasus/products/model/formatter"
], function (controller , formatter) {
	"use strict";
	
	QUnit.module("Test for Formatter ");
    QUnit.test("Should format the price", function(assert){
   var sFormattedPrice= formatter.appendCurrency("42.0000");	
   assert.equal(sFormattedPrice, "42.00" ,"The price is formatted to 2 decimal");
  
    });
});