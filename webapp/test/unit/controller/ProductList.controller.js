/*global QUnit*/

sap.ui.define([
	"parasus/products/controller/ProductList.controller"
], function (Controller) {
	"use strict";

	QUnit.module("ProductList Controller");
	var oAppController = new Controller();
	QUnit.test(" Test Error hanler the ProductList controller", function (assert) {
	
		// oAppController.onInit();
		oAppController.errorHandler();
	
		assert.ok(oAppController,"Product List Controller success");
	});
	QUnit.test("Test for sum function", function(assert){
		var iSum = oAppController.sum(1,2);
		assert.strictEqual(iSum, 3, "actual:"+iSum+ ", expected:3");
	});

});