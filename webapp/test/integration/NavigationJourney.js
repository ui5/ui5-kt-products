	/*global QUnit*/

	sap.ui.define([
		"sap/ui/test/opaQunit",
		"./pages/ProductList"
	], function (opaTest) {
		"use strict";

		QUnit.module("Test filters");

		opaTest("Should see the initial page of the app", function (Given, When, Then) {
			// Arrangements
			Given.iStartMyApp();

			// Assertions
			Then.onTheAppPage.iShouldSeeTheApp();

			
		});
		opaTest("Should filter the list for product id 1",function(Given , When, Then){
			//Action
			When.onTheAppPage.iFilterWithId("idProductIdInput","1").and.iPressOnGo("idGoBtn");
			
			// Assertions
			Then.onTheAppPage.iShouldSeeFilteredTable("idTable");
			
	        //Cleanup
			Then.iTeardownMyApp();
		});
	});