sap.ui.define([
	"sap/ui/test/Opa5",
	'sap/ui/test/actions/EnterText',
	'sap/ui/test/actions/Press'
], function (Opa5, EnterText, Press) {
	"use strict";
	var sViewName = "ProductList";
	Opa5.createPageObjects({
		onTheAppPage: {

			actions: {
				iFilterWithId: function (sId, val) {
					return this.waitFor({
						id: sId,
						viewName: sViewName,
						actions: new EnterText({
							text: val
						}),
						errorMessage: "Did not find ID filter"
					});
				},

				iPressOnGo: function (sId) {
					return this.waitFor({
						id: sId,
						viewName: sViewName,
						actions: new Press(),
						errorMessage: "Did not find Go Btn"
					});
				}
			},
			assertions: {

				iShouldSeeTheApp: function () {
					return this.waitFor({
						id: "dynamicPageId",
						viewName: sViewName,
						success: function () {
							Opa5.assert.ok(true, "The ProductList view is displayed");
						},
						errorMessage: "Did not find the ProductList view"
					});
				},
				iShouldSeeFilteredTable: function (sId) {
					return this.waitFor({
						id: sId,
						viewName: sViewName,
						success: function (oTable) {
							Opa5.assert.equal(1, oTable.getItems().length, "the table was filtered successfully");
						},
						errorMessage: "Did not find table"
					});
				}
			}
		}
	});

});