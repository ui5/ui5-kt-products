sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"parasus/products/model/formatter"
], function (Controller , formatter) {
	"use strict";

	return Controller.extend("parasus.products.controller.ProductDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf parasus.products.view.ProductDetail
		 */
		 	formatter: formatter,
		onInit: function () {
        this.getOwnerComponent().getRouter().getRoute("RouteProductDetail").attachMatched(this._onRouteMatched,this);
		},
		_onRouteMatched: function(oEvent) {
		  var productId = oEvent.getParameter("arguments").productId;
		  this.getView().bindElement( "/Products(" + productId + ")" );
		  //var oJsonModel = new sap.ui.model.json.JSONModel(this.getView().getElementBinding()
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf parasus.products.view.ProductDetail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf parasus.products.view.ProductDetail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf parasus.products.view.ProductDetail
		 */
		//	onExit: function() {
		//
		//	}

	});

});