sap.ui.define([
			"sap/ui/core/mvc/Controller",
			"sap/ui/model/Filter",
			"sap/ui/model/FilterOperator",
			"parasus/products/model/formatter",
			"sap/ui/core/Fragment",
			"sap/m/Token"
		], function (Controller, Filter, FilterOperator, formatter, Fragment, Token) {
			"use strict";

			return Controller.extend("parasus.products.controller.ProductList", {
					formatter: formatter,
					onInit: function () {
						var oData = {
							busy: true
						};
						var oJsonModel = new sap.ui.model.json.JSONModel(oData);
						this.getView().setModel(oJsonModel, "jsonModel");
						//this.getView().setModel(oJsonModel ,"busyIndModel");
						//this.getView().setModel("busyIndModel").setProperty("/busy", false);

					},
					onProductIdVHRequest: function () {

						//// Fragment
						this._multiInput = this.getView().byId("idProductIdInput");
						var oView = this.getView();
						var that = this;
						if (!this.byId("valHelp")) {
							Fragment.load({
								id: oView.getId(),
								name: "parasus.products.fragment.ProductNameVH",
								controller: this
							}).then(function (oDialog) {
								// 		// connect dialog to the root view of this component (models, lifecycle)
								oView.addDependent(oDialog);
								that.fetchData();
								oDialog.open();
							});
						} else {

							// this.fetchData();
							this.byId("valHelp").open();
						}
						// End
						// if (!this._oValueHelpDialog) {
						// 	this._oValueHelpDialog = sap.ui.xmlfragment("parasus.products.fragment.ProductNameVH", this);
						// }

						// this.getView().addDependent(this._oValueHelpDialog);
						// /// Bind Value help table
						// this.getView().getModel().read("/Products" ,{
						// 	success: this.bindTable.bind(this),
						// 	error: this.errorHandler
						// });

						// this._oValueHelpDialog.open();

					},
					onProductNameVHRequest: function () {
						this._multiInput = this.getView().byId("idProductNameInput");
						var oView = this.getView();
						var that = this;
						if (!this.byId("valHelp")) {
							Fragment.load({
								id: oView.getId(),
								name: "parasus.products.fragment.ProductNameVH",
								controller: this
							}).then(function (oDialog) {
								// 		// connect dialog to the root view of this component (models, lifecycle)
								oView.addDependent(oDialog);
								that.fetchData();
								oDialog.open();
							});
						} else {

							// this.fetchData();
							this.byId("valHelp").open();
						}
					},
					////// Value help related code

					onValueHelpCancelPress: function (oEvent) {
						debugger;
						// this._oValueHelpDialog.close();
						this.byId("valHelp").close();
					},
					onValueHelpOkPress: function (oEvent) {
						debugger;
						var aTokens = oEvent.getParameter("tokens");

						this._multiInput.setTokens(aTokens);
						// this.getView().byId("idProductNameInput").setTokens(aTokens);
						this.byId("valHelp").close();

					},
					onValueHelpAfterClose: function () {
						//this.byId("valHelp").destroy();
					},
					fetchData: function () {

						this.getView().getModel().read("/Products", {
							success: this.bindTable.bind(this),
							error: this.errorHandler
						});
					},
					bindTable: function (data) {
						debugger;
						var oVhModel = new sap.ui.model.json.JSONModel(data, "vhModel");

						// bind Value help table
						// this._oValueHelpDialog.getTableAsync().then(function (oTable) {
						this.byId("valHelp").getTableAsync().then(function (oTable) {
							oTable.setModel(oVhModel);
							oTable.addColumn(new sap.ui.table.Column({
								label: "Product ID",
								template: "ProductID"
							}));
							oTable.addColumn(new sap.ui.table.Column({
								label: "Product Name",
								template: "ProductName"
							}));
							oTable.bindRows("/results/");
							// this._oValueHelpDialog.update();
							this.byId("valHelp").update();
						}.bind(this));

					},
					errorHandler: function () {
						console.log("some error occured while fetching data");
					},
					sum: function (a, b) {
						return a + b;
					},
					evenOdd: function (a) {
						if (a % 2 == 0) {

						}
					},

					/////// Code ends
					onProductIDSubmit: function (oEvent) {
						// var sVal = oEvent.getSource().getValue();
						// // var oFilter = new sap.ui.model.Filter();
						// var oFilter = new Filter({
						// 	path: 'ProductID',
						// 	operator: FilterOperator.EQ,
						// 	value1: sVal
						// });
						// var oTable = this.getView().byId("idTable");
						//      oTable.getBinding("items").filter(oFilter);
					},
					onPressGo: function () {
						debugger;
						// var sVal = oEvent.getSource().getValue();
						// var oFilter = new sap.ui.model.Filter();
						var tokens = this.getView().byId("idProductIdInput").getTokens();
						var sVal = this.getView().byId("idProductIdInput").getValue();
						var nameVal = this.getView().byId("idProductNameInput").getValue();
						var nameTokens = this.getView().byId("idProductNameInput").getTokens();
						var filters = [];
						var oFilter1, oFilter2;
						var oFilterId;
						var oFilterName;

						//////////// Product ID
						if (tokens.length > 0 & nameTokens.length === 0) {

							tokens.forEach(function (tokenId) {
								var vkey = tokenId.getKey();
								oFilter1 = new Filter({
									path: 'ProductID',
									operator: FilterOperator.EQ,
									value1: vkey
								});
								filters.push(oFilter1);
							});

							if (sVal != "") {
								var oFilterVal = new Filter({
									path: 'ProductID',
									operator: FilterOperator.EQ,
									value1: sVal
								});
								filters.push(oFilterVal);
							}
						}

							////////////// Product Name
							else if (nameTokens.length > 0 & tokens.length === 0) {
								nameTokens.forEach(function (tokenName) {
									var sName = tokenName.getText().split(" (")[0];

									oFilter2 = new Filter({
										path: 'ProductName',
										operator: FilterOperator.Contains,
										value1: sName
									});
									filters.push(oFilter2);
								});
							}

							///////////// End

							// if (sVal != "") {
							// var oFilter = new Filter({
							// 	path: 'ProductID',
							// 	operator: FilterOperator.EQ,
							// 	value1: sVal
							// });
							// filters.push(oFilter);
							else {
								var filterNewId = this.searchForId(tokens, oFilterId);
								var filterNewName = this.searchForName(nameTokens, oFilterName);
								var oFilter = new Filter({
									filters: [
										new Filter(filterNewId),
										new Filter(filterNewName)
									],
									and: true
								});
							}

							// var oFilter = new Filter({
							// 	path: 'ProductID',
							// 	operator: FilterOperator.EQ,
							// 	value1: sVal
							// });

							// } else if (sVal === "") {
							// 	var oFilter = new Filter({
							// 		path: 'ProductName',
							// 		operator: FilterOperator.Contains,
							// 		value1: nameVal
							// 	});
							// } else {
							// 	var oFilter = new Filter({
							// 		filters: [

							// 			new Filter({
							// 				path: 'ProductID',
							// 				operator: FilterOperator.EQ,
							// 				value1: sVal
							// 			}),
							// 			new Filter({
							// 				path: 'ProductName',
							// 				operator: FilterOperator.Contains,
							// 				value1: nameVal
							// 			})

							// 		],
							// 		and: true | false
							// 	});
							// }

							var oTable = this.getView().byId("idTable");
							// oTable.getBinding("items").filter(oFilter);

							if (nameTokens.length > 0 & tokens.length > 0) {
								oTable.getBinding("items").filter(oFilter);
							} else {
								oTable.getBinding("items").filter(filters);
							}

						},
						searchForId: function (tokens, oFilterId) {
								debugger;
								var filterNewId = [];
								tokens.forEach(function (tokenId) {
									var vkey = tokenId.getKey();
									oFilterId = new Filter({
										path: 'ProductID',
										operator: FilterOperator.EQ,
										value1: vkey
									});
									filterNewId.push(oFilterId);
								});
								return filterNewId;
							},
							searchForName: function (nameTokens, oFilterName) {
								debugger;
								var filterNewName = [];
								nameTokens.forEach(function (tokenName) {
									var sName = tokenName.getText().split(" (")[0];

									oFilterName = new Filter({
										path: 'ProductName',
										operator: FilterOperator.Contains,
										value1: sName
									});
									filterNewName.push(oFilterName);
								});
								return filterNewName;
							},
							onPressClear: function(){
								debugger;
								this.getView().byId('idProductNameInput').removeAllTokens();
							},
							onRowClick: function (oEvent) {
								var sProductId = oEvent.getParameter("listItem").getAggregation("cells")[0].getText();
								this.getOwnerComponent().getRouter().navTo("RouteProductDetail", {
									productId: sProductId
								});
							}
					});
			});